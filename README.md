# tifu_ng

Nuevo tifu, más poder, más desacoplao, puede que más Go

## Comandos actuales

Ahora mismo tifu tiene estos comandos:

* AddQuote: Añade una nueva quote a la BBDD sin validar
* Anclado: Envía siempre el mensaje '¡El que tengo aquí colgado! 🍆'
* Canal: Envía la descripción del canal o una bordería
* Categorias: Muestra las categorías con citas validadas
* ChatId: Devuelve el ID del canal actual
* Help: Muestra los comandos a los que responde el bot
* Quote: Muestra una cita aleatoria, opcionalmente filtrada por categoría
* Repo: Muestra el repo antiguo
* Stats: Muestra estadísticas del canal (dichas, no dichas y no validadas)
* Version: Muestra la versión del bot
* Web: Muestra la web asociada al canal o un mensaje de error en caso de no existir
* Invite: Muestra el enlace de invitación del canal

## Mejoras

Aquí documentaremos los nuevos comandos que se vayan a realizar o las mejoras sobre los actuales

### Mejoras

1. Dotar al comando Quote de más inteligencia, de modo que tenga el cuenta el contexto para seleccionar la Quote

### Comandos nuevos

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/bofhers/tifu_ng/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***